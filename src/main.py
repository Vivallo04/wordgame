from util.Util import *
from states.StateMachine import *
import pygame
import sys


class Game:

    def __init__(self):
        pygame.init()

        self.util = Util()

        # Initialize Game screen
        self.screen_width = 1280
        self.screen_height = 720
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode(size=(self.screen_width, self.screen_height))
        self.sprites = pygame.sprite.Group()
        pygame.display.set_caption("Word Game")

        # instance of state machine
        self.GState_machine = StateMachine(self)

        # TODO: SOLVE THE WORD, HINTS (DICTIONARY DEFINITIONS), selected tiles animation

        self.current_state = self.GState_machine.return_state(0)

        # User events
        pygame.time.set_timer(pygame.USEREVENT, 1000)

    # Main Game loop
    def main(self):
        print("Current game state: ", self.current_state)

        while True:
            for event in pygame.event.get():
                if event.type == pygame.USEREVENT:
                    if self.current_state.__class__.__name__ == 'GameState':
                        if self.current_state.timer <= 0:
                            self.current_state = self.GState_machine.return_state(3)
                        else:
                            self.current_state.decrease_timer()
                    if self.current_state.__class__.__name__ == 'CountDownState':
                        if self.current_state.counter <= 0:
                            self.current_state = self.GState_machine.return_state(2)
                        else:
                            self.current_state.start_countdown()


                if event.type == pygame.KEYDOWN:

                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                        sys.exit()

                    if self.current_state.__class__.__name__ == 'GameState':
                        if event.key in range(pygame.K_a, pygame.K_z + 1):
                            self.current_state.edit_user_entry(event.unicode)

                        if event.key == pygame.K_BACKSPACE:
                            self.current_state.pop_string()

                if event.type == pygame.MOUSEBUTTONDOWN:
                    for button in self.sprites:
                        button.handle_event(event)

                if event.type == pygame.QUIT:
                    sys.exit()


            self.GState_machine.render_state()
            pygame.display.flip()
            self.clock.tick(144)
            pygame.display.update()


if __name__ == '__main__':
    Game1 = Game()
    Game1.main()
