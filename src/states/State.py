from enum import Enum
from .MenuState import *
from .GamerOverState import *
from .game_state.GameState import *
from .StateMachine import *

class State(Enum):
    MAIN_MENU = 1,
    COUNT_DOWN = 2,
    GAME_STATE = 3,
    GAME_OVER = 4,
    GAME_WON = 5