import pygame


class CountDownState:

    def __init__(self, game):
        self.game = game
        self.counter = 5

    def start_countdown(self):
        self.counter -= 1

    def render(self):
        self.game.screen.fill(self.game.util.turquoise)

        self.game.screen.blit(self.game.util.font_extra_large.render(str(self.counter), 0, self.game.util.yellow),
                              (self.game.screen_width / 2 - 30, self.game.screen_height / 2 - 100))
