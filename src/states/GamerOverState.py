import pygame
from src.util.Button import Button


class GameOver:

    def __init__(self, game):
        self.game = game

        # load assets - Fonts
        self.font_extra_large = game.util.load_font("font.ttf", 128)
        self.font_large = game.util.load_font("font.ttf", 64)
        self.font_medium = game.util.load_font("font.ttf", 32)
        self.font_small = game.util.load_font("font.ttf", 16)

    def render(self):
        self.game.screen.fill(self.game.util.white)
        self.background_image = self.game.util.load_asset("background.png")
        self.background_image = pygame.transform.scale(self.background_image, (1280, 720)).convert()
        self.game.screen.blit(self.background_image, (0, 0))

        # game over label
        game_over_label = Button("Game Over", self.game, self.game.util.white,
                                 self.game.screen_width / 2 - 150, 280, 305, 50, 0, 70)

        # hellouttons
        self.exit_button = Button("Exit", self.game, self.game.util.white,
                                  self.game.screen_width / 2 - 150, 350, 305, 50, 4, 130)

        self.game.sprites.add(self.exit_button)
