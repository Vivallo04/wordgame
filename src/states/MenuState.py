import pygame
from util.Button import *


class MenuState:

    def __init__(self, game):
        self.game = game
        self.name = 'menu_state'


    def render(self):
        # Initialize background
        self.game.screen.fill(self.game.util.white)
        self.background_image = self.game.util.load_asset("background.png")
        self.background_image = pygame.transform.scale(self.background_image, (1280, 720)).convert()
        self.game.screen.blit(self.background_image, (0, 0))
        self.game.screen.blit(self.game.util.font_large.render("WORD GAME", 0, (0, 0,0)), (self.game.screen_width - 800, self.game.screen_height - 600))


        self.start_button = Button("Start Game", self.game, self.game.util.white,
                              self.game.screen_width/ 2 - 100, 290, 200, 50, 1, 10)

        self.exit_button = Button("Exit", self.game, self.game.util.white,
                             self.game.screen_width / 2 - 100, 350, 200, 50, 4, 70)

        self.game.sprites.add(self.start_button, self.exit_button)



        pygame.display.flip()