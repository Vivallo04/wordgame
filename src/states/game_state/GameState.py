import random
from src.util.Button import *

from src.states.game_state.WordGenerator import *
import pygame

class GameState:

    def __init__(self, game):
        self.game = game
        self.active_state = False
        self.timer = 3
        self.score = 0
        self.level = 1
        self.WordGenerator = WordGenerator()

        # load assets - Fonts
        self.font_extra_large = game.util.load_font("font.ttf", 128)
        self.font_large = game.util.load_font("font.ttf", 64)
        self.font_medium = game.util.load_font("font.ttf", 32)
        self.font_small = game.util.load_font("font.ttf", 16)

        # images
        self.tile = game.util.load_asset("tile.png")
        self.TILE_SIZE = 65

        self.selected_tile = game.util.load_asset("selectedTile.png")

        self.current_entry_len = self.WordGenerator.get_entry_len()

        # selected words
        self.queued_words = []
        self.shuffled_string = None
        self.user_entry = ""

        # Generate the entries once the game starts
        self.get_random_words()

        # select one word from the queue
        self.current_word = self.select_word_from_queue()
        self.shuffled_string = self.shuffle_string(self.current_word)

        self.polygon_color1 = self.game.util.yellow

    def get_user_entry(self):
        return self.user_entry

    def edit_user_entry(self, entry):
        if self.user_entry in self.queued_words:
            self.user_entry = ""
            self.remove_word_from_queue()
        else:
            self.user_entry += entry
            print(self.user_entry)

    def render_entries(self, letter_len):
        for i in range(0, letter_len):
            self.game.screen.blit(self.font_extra_large.render("_", 0, self.game.util.white),
                                  (self.game.screen_width - 1170 + (i * (self.TILE_SIZE * 2)), self.game.screen_height - 550))
        # print("Rendered: ", letter_len, " spaces")

    def render_user_entry_text(self):
        # render user text -------------------------------------------------------------------------------
        for letter in range(0 , len(self.user_entry)):
            self.game.screen.blit(self.font_large.render(self.user_entry[letter], 10, self.game.util.white),
                                  (self.game.screen_width - 1160 + (letter * 135), self.game.screen_height - 530))


    def get_random_words(self):
        current_entry = self.WordGenerator.get_random_key()
        print("Current entry: ", current_entry)

        for word in self.WordGenerator.word_bank[current_entry]:
            self.queued_words.append(word)
        print("Word on queue: ", self.queued_words)
        return self.queued_words

    def select_word_from_queue(self):
        if len(self.queued_words) == 0:
            return self.game.GState_machine.set_game_state(4)
        print("Current word playing: ", self.queued_words[0])
        print("Current word len: " , len(self.queued_words[0]))
        print("Rendering: " , len(self.queued_words[0]), " spaces")
        return self.queued_words[0]

    def remove_word_from_queue(self):
        self.queued_words.pop(0)

    def reset_timer(self):
        self.timer = 60

    def get_timer(self):
        return self.timer

    def pop_string(self):
        self.user_entry = self.user_entry[:-1]
        print(self.user_entry)
        return self.user_entry

    def decrease_timer(self):
        self.timer -= 1

    def render_UI(self):
        # Random words                                                       x,  y,   w,  h    l

        pygame.draw.rect(self.game.screen, self.game.util.yellow, pygame.Rect(self.game.screen_width - 1259,
                                                                            self.game.screen_height - 700, 799, 399))

        pygame.draw.rect(self.game.screen, self.game.util.white, pygame.Rect(self.game.screen_width - 1260,
                                                                             self.game.screen_height - 700, 800, 400),
                         4, border_radius=5)

        # Player score and timer
        pygame.draw.rect(self.game.screen, self.game.util.light_green, pygame.Rect(self.game.screen_width - 430,
                                                                             self.game.screen_height - 700,
                                                                             self.game.screen_width - 869,
                                                                             self.game.screen_height - 499))
        pygame.draw.rect(self.game.screen, self.game.util.white, pygame.Rect(self.game.screen_width - 430,
                                                                             self.game.screen_height - 700,
                                                                             self.game.screen_width - 870,
                                                                             self.game.screen_height - 500),
                                                                             4, border_radius = 5)

        # Hints
        pygame.draw.rect(self.game.screen, self.game.util.yellow2, pygame.Rect(self.game.screen_width - 430,
                                                                             self.game.screen_height - 460,
                                                                             self.game.screen_width - 870,
                                                                             self.game.screen_height - 280))

        pygame.draw.rect(self.game.screen, self.game.util.white, pygame.Rect(self.game.screen_width - 430,
                                                                             self.game.screen_height - 460,
                                                                             self.game.screen_width - 870,
                                                                             self.game.screen_height - 280),
                         4, border_radius=5)

        # Keyboard
        pygame.draw.rect(self.game.screen, self.game.util.white, pygame.Rect(self.game.screen_width - 1260,
                                                                             self.game.screen_height - 280,
                                                                             self.game.screen_width - 479,
                                                                             self.game.screen_height - 459))

        pygame.draw.rect(self.game.screen, self.game.util.white, pygame.Rect(self.game.screen_width - 1260,
                                                                             self.game.screen_height - 280,
                                                                             self.game.screen_width - 480,
                                                                             self.game.screen_height - 460),
                                                                             4, border_radius = 5)

    def shuffle_string(self, word):
        letters = list(word)
        random.shuffle(letters)
        self.shuffled_string = "".join(letters)
        print("Shuffled string: ", str(self.shuffled_string))
        return self.shuffled_string

    def render_letter_keys(self):
        pos_x_letters = self.game.screen_width - 1235
        pos_x_tiles = self.game.screen_width - 1250
        pos_y_tiles = self.game.screen_height - 265
        pos_y_letters = self.game.screen_height - 260
        for i in range(0, 12):
            # render tiles
            self.game.screen.blit(self.tile, (pos_x_tiles + self.TILE_SIZE * i, pos_y_tiles))

            # render letters
            self.game.screen.blit(self.font_large.render(self.WordGenerator.letters[i].upper(), 0, self.game.util.black),
                                  (pos_x_letters + self.TILE_SIZE * i, pos_y_letters))

        for i in range(13, len(self.WordGenerator.letters) - 1):
            for j in range (0, len(self.WordGenerator.letters) - 13):
                # render tiles
                self.game.screen.blit(self.tile, ((pos_x_tiles + self.TILE_SIZE * i) - 845, pos_y_tiles + self.TILE_SIZE))
                # render letters
                self.game.screen.blit(self.font_large.render(self.WordGenerator.letters[i].upper(), 0, self.game.util.black),
                                      ((pos_x_letters + self.TILE_SIZE * i) - 845, pos_y_letters + 64))

        self.game.screen.blit(self.tile, (self.game.screen_width - 1250, pos_y_tiles + 128))
        self.game.screen.blit(self.font_large.render(self.WordGenerator.letters[len(self.WordGenerator.letters) - 1].upper(), 0, self.game.util.black),
                              ((self.game.screen_width - 1235), pos_y_tiles + 132))

    def render_entry_polygon(self):
        pygame.draw.polygon(surface=self.game.screen, color=self.game.util.game_background,
                            points=[(90, self.game.screen_height - 505),
                                    (80, self.game.screen_height - 510),
                                    (80, self.game.screen_height - 500)])

    def render(self):
        # Set the background
        self.game.screen.fill(self.game.util.turquoise)
        self.render_UI()
        self.render_letter_keys()

        # render score & score label
        self.game.screen.blit(self.font_small.render("SCORE", 0, self.game.util.black),
                              (self.game.screen_width - 90, self.game.screen_height - 690))
        self.game.screen.blit(self.font_large.render(str(self.score), 0, self.game.util.black),
                              (self.game.screen_width - 90, self.game.screen_height - 680))

        # render timer & timer label
        self.game.screen.blit(self.font_small.render("TIMER", 0, self.game.util.black),
                              (self.game.screen_width - 402, self.game.screen_height - 690))
        self.game.screen.blit(self.font_large.render(str(self.get_timer()), 0, self.game.util.black),
                              (self.game.screen_width - 415, self.game.screen_height - 680))

        # render level label
        self.game.screen.blit(self.font_small.render("LEVEL", 0, self.game.util.black),
                              (self.game.screen_width - 410, self.game.screen_height - 560))
        self.game.screen.blit(self.font_large.render(str(self.level), 0, self.game.util.black),
                              (self.game.screen_width - 402, self.game.screen_height - 550))
        # render hint Text
        self.game.screen.blit(self.font_medium.render("Hints!", 0, self.game.util.black),
                              (self.game.screen_width - 415, self.game.screen_height - 450))

        # current shuffled word
        self.game.screen.blit(self.font_small.render("CURRENT WORD", 0, self.game.util.black),
                              (self.game.screen_width - 180, self.game.screen_height - 560))
        self.game.screen.blit(self.font_large.render(self.shuffled_string, 0, self.game.util.black),
                              (self.game.screen_width - 210, self.game.screen_height - 550))

        # render the space for the user entry
        self.render_entries(len(self.current_word))

        self.render_user_entry_text()

        # render user entry polygon
        self.render_entry_polygon()

        pygame.display.flip()