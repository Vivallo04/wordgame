import pygame
import random
import json


class WordGenerator:

    def __init__(self):
        # File containing the word bank
        json_file = open("res/files/word_bank.json")

        # Add the words from the json file to dictionary
        self.word_bank = json.load(json_file)
        self.letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g',
                        'h', 'i', 'k', 'j', 'l', 'm', 'n',
                        'o', 'p', 'q', 'r', 's', 't', 'u',
                        'v', 'w', 'x', 'y', 'z']

        # Close the json file
        json_file.close()

    def get_random_key(self):
        entries = list(dict(self.word_bank).keys())
        random_entry = random.choice(entries)
        return random_entry

    def get_entry_len(self):
        return len(self.get_random_key())






