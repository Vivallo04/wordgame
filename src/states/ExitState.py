import pygame
import sys


class ExitState:

    def __int__(self):
        pygame.quit()

    def render(self):
        pygame.quit()
        sys.exit()
