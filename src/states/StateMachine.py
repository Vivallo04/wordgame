import pygame
from .MenuState import *
from .State import *
from src.states.game_state.GameState import GameState
from src.states.CountDownState import *
from .GamerOverState import *
from .ExitState import *

class StateMachine:

    def __init__(self, game):
        self.game = game
        self.GameState = GameState(self.game)
        self.MenuState = MenuState(self.game)
        self.CountDownState = CountDownState(self.game)
        self.GameOverState = GameOver(self.game)
        self.ExitState = ExitState()
        self.states = [ self.MenuState, self.CountDownState, self.GameState,
                        self.GameOverState, self.ExitState]


    def return_state(self, pos):
        return self.states[pos]

    def render_state(self):
        return self.game.current_state.render()

    def set_game_state(self, state):
        if state == State.GAME_STATE:
            self.game.current_state = self.states[0]
        elif state == State.MAIN_MENU:
            self.game.current_state = self.states[1]
        elif state == State.GAME_OVER:
            self.game.current_state = self.states[2]
        else:
            raise pygame.error
