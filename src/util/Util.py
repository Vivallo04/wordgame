import pygame
import sys
import os

class Util:

    def __init__(self):
        # colours
        self.game_background = 48, 57, 86
        self.white = 255, 255, 255
        self.red = 255, 0, 0
        self.yellow = 255, 159, 28
        self.yellow2 = 25, 191, 105
        self.light_green = 203, 243, 240
        self.turquoise = 46, 196, 182
        self.black = 0, 0, 0
        self.font_extra_large = self.load_font("font.ttf", 128)
        self.font_large = self.load_font("font.ttf", 64)
        self.font_medium = self.load_font("font.ttf", 32)
        self.font_small = self.load_font("font.ttf", 16)

    # Load the asset on the specified path
    def load_asset(self, file_name):

        fullname = os.path.join('res', 'graphics', file_name)
        try:
            image = pygame.image.load(fullname)
            if image.get_alpha() == None:
                image = image.convert()
            else:
                image = image.convert_alpha()

        except pygame.error as message:
            print("Unable to load the file: ", fullname)
            raise SystemExit(message)

        return image

    def load_font(self, file_name, font_size):
        fullname = os.path.join('res', 'fonts', file_name)
        try:
            font = pygame.font.Font(fullname, font_size)

        except pygame.error as message:
            print("Unable to load the font: ", fullname)
            raise SystemExit(message)

        return font

    def read_file(self, file_name):
        file_path = os.path.join('res', 'files')
        try:
            file = open("word_bank.json")

        except pygame.error as message:
            print("Unable to open the file: ", file_path)
            raise SystemExit(message)
        return file