import pygame.draw
import pygame

class Button(pygame.sprite.Sprite):

    def __init__(self, text, game, color, x, y, w, h, type, offset_x):
        super().__init__()

        # load assets - Fonts
        self.action_type = type
        self.offset_x = offset_x
        self.pos_x = x
        self.pos_y = y
        self.widht = w
        self.height = h
        self.game = game
        self.font_extra_large = self.game.util.load_font("font.ttf", 128)
        self.font_large = self.game.util.load_font("font.ttf", 64)
        self.font_medium = self.game.util.load_font("font.ttf", 32)
        self.font_small = self.game.util.load_font("font.ttf", 16)
        self.rectangle = pygame.draw.rect(self.game.screen, color, (x, y, w, h))
        self.text = self.font_medium.render(text, 0, (0, 0, 0))
        self.render()

    def trigger_state_change(self):
        self.game.GState_machine.return_state(self.action_type)

    def render(self):
        self.game.screen.blit(self.text, (self.pos_x + self.offset_x, self.pos_y + 10))

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_position = pygame.mouse.get_pos()

            if self.rectangle.collidepoint(mouse_position):
                self.game.current_state = self.game.GState_machine.return_state(self.action_type)

